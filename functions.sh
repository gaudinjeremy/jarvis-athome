#!/bin/bash

#MODULE AT HOME pour savoir qui est à la maison

js_ah_whoAtHome(){

	group="athome"

    row=$(js_ia_loopCell $group)
    if [ $row != 'false' ]
    then
        nb=$(js_ia_countCells $row)
		present=0
        for ((i=1; i<=$nb; i++))
        do
            name=$(js_ia_getCell $row $i)
			ping=$(js_ping_checkIp $name 2)

            if [ $ping = "true" ]
            then
                present=$(($present + 1))

				if [ $present = 1 ]
				then
					personnes="$name"
				else
					personnes="$personnes $name"
				fi
            fi
		done

		if [ $present = 0 ]
        then
            msg="Il n'y à personne à la maison"
			if [ $1 = 'false' ]
	        then
		        say "$msg"
	        fi
        else
        	msg="je detecte $personnes à la maison"
			if [ $1 = 'false' ]
	        then
		        say "$msg"
	        fi
        fi

		if [ $1 = 'true' ]
		then
			oldPresence=$(js_ia_getStrictText presents 2)

			if [ $oldPresence != $present ]
			then
				js_ia_sendToMe "$msg"
				js_ia_upCell $oldPresence $present presents
				js_ah_switchCameras $present
				js_ah_autosound $present
			fi
		fi
	else
		if [ $1 = 'false' ]
        then
			js_ia_say "erreur"
	        say "je n'ai pas trouvé dans ma mémoire"
        else
			js_ia_sendToMe "Erreur dans la fonction whoAtHome()"
        fi
	fi
}

js_ah_switchCameras(){

	autocameras=$(js_ia_getStrictText autocameras 2)

	if [ $autocameras = "on" ]
	then
		if [ $1 = 0 ]
	    then
			js_rf_switchGroup "cameras" "on" >/dev/null
			js_ia_sendToMe "J'ai allumé les caméras"
		else
			js_rf_switchGroup "cameras" "off" >/dev/null
			js_ia_sendToMe "J'ai éteins les caméras"
		fi
	else
		if [ $1 = 0 ]
		then
			js_ia_sendToMe "Il faudrai allumer les caméras"
		fi
	fi
}

js_ah_autosound(){

    if [ $1 = 0 ]
    then
        autosound=$(js_ia_getStrictText autosound 2)
        if [ $autosound = "on" ]
        then
            voice=$(js_ia_getStrictText voice 2)
            if [ $voice != 0 ]
            then
                js_sd_setSound 0
                js_sd_setState voice 0
            fi

            micro=$(js_ia_getStrictText micro 2)
            if [ $micro != 0 ]
            then
                js_sd_setMic 0
                js_sd_setState micro 0
            fi
        fi
    else
        autosound=$(js_ia_getStrictText autosound 2)
        if [ $autosound = "on" ]
        then
            voice=$(js_ia_getStrictText voice 2)
            if [ $voice = 0 ]
            then
                autovoice=$(js_ia_getStrictText autovoice 2)
                js_sd_setSound $autovoice
                js_sd_setState voice $autovoice
            fi

            micro=$(js_ia_getStrictText micro 2)
            if [ $micro = 0 ]
            then
                automicro=$(js_ia_getStrictText automicro 2)
                js_sd_setMic $automicro
                js_sd_setState micro $automicro
            fi
        fi
    fi
}
